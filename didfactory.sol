// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.12;

import "./safemath.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract DidFactory {

    using SafeMath for uint256;
    using SafeMath32 for uint32;

    uint16 idDigits = 5;
    uint idModulus = 10 ** idDigits;
    uint32 nonce = 0;
    uint32 registry = 10000;

    struct Did {
        string scheme;
        string method;
        string path;
        string property;
        uint256 registrationNo;
        string did;
    }

    Did[] internal dids;
    Did[] internal devicedids;
    Did[] internal softhubdids;
    
    mapping (address => uint) public identityToAccount;
    mapping (address => bool) internal accountIsRegistered;

    function createdid(string memory _property) public {
        require(accountIsRegistered[msg.sender] != true);
        accountIsRegistered[msg.sender] = true;
        _createDid(_property);
    }

    function _createDid(string memory _property) internal {
        uint256 _randRegistry = _generateRandomIdentifier();
        registry++;
        string memory did = string.concat("did:","ethblockchain:","/application/mscres-52/",_property,Strings.toString(_randRegistry));
        dids.push(Did("did", "ethblockchain", "/application/mscres-52/", _property, _randRegistry, did));
        identityToAccount[msg.sender] = dids.length - 1;
    }

    function _createDeviceDid() internal returns(uint){
        uint256 _randRegistry = _generateRandomIdentifier();
        registry++;
        string memory did = string.concat("did:","ethblockchain:","/application/mscres-52/","device",Strings.toString(_randRegistry));
        devicedids.push(Did("did", "ethblockchain", "/application/mscres-52/", "device", _randRegistry, did));
        return devicedids.length - 1;
    }

    function _createSoftHubDid() internal returns(uint){
        uint256 _randRegistry = _generateRandomIdentifier();
        registry++;
        string memory did = string.concat("did:","ethblockchain:","/application/mscres-52/","softhub",Strings.toString(_randRegistry));
        softhubdids.push(Did("did", "ethblockchain", "/application/mscres-52/", "softhub", _randRegistry, did));
        return softhubdids.length - 1;
    }

    function _generateRandomIdentifier() internal returns(uint) {
        nonce++;
        return uint(keccak256(abi.encodePacked(block.timestamp, msg.sender, nonce))) % idModulus;
    }

    function didProperty() public view returns (string memory) {
        return dids[identityToAccount[msg.sender]].did;
    }

}