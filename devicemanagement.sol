// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.12;

import "./iothubFactory.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract DeviceManagement is IothubFactory{

    function claimDevice(string memory _deviceRN) external {
        // check to see if device is unclaimed.
        require(devices[rnToDev[_deviceRN]].ownerDID == 0);
        require(devToHub[rnToDev[_deviceRN]] == 0);

        // complete the claim process.
        devices[rnToDev[_deviceRN]].ownerDID = identityToAccount[msg.sender];
        devToHub[rnToDev[_deviceRN]] = ownerToHub[msg.sender];
    }

    function changeHub(string memory _deviceRN, address _targetOwner) external {
        // ckeck if the device belongs to the hub the sender is administrator of.
        require(ownerToHub[msg.sender] == devToHub[rnToDev[_deviceRN]]);

        //bind the device to the next owner
        devices[rnToDev[_deviceRN]].ownerDID = identityToAccount[_targetOwner];
        devToHub[rnToDev[_deviceRN]] = 0; // remember, hub 0 is always the one of the contract.
    }

    function claimTransferedDevice(string memory _deviceRN) external {
        // check if the hub which the sender is administrator of is the same with the one the device belongs to.
        require(keccak256(abi.encodePacked(dids[identityToAccount[msg.sender]].property)) == keccak256(abi.encodePacked("administrator")));

        // check if the binded address of the device is the same as the sender.
        require(identityToAccount[msg.sender] == devices[rnToDev[_deviceRN]].ownerDID);

        //complete the transfer.
        devices[rnToDev[_deviceRN]].ownerDID = identityToAccount[msg.sender];
        devToHub[rnToDev[_deviceRN]] = ownerToHub[msg.sender];
    }

    function removeFromHub(string memory _deviceRN) external {
        // ckeck if the device belongs to the hub the sender is administrator of.
        require(ownerToHub[msg.sender] == devToHub[rnToDev[_deviceRN]]);

        // unclaim the device.
        devices[rnToDev[_deviceRN]].ownerDID = 0; //the did 0 is the one of the contract
        devToHub[rnToDev[_deviceRN]] = 0; //hub 0 is always the one of the contract
    }

    
}