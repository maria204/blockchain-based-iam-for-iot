// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.12;

import "./devicemanagement.sol";

contract Verification is DeviceManagement {

    function D1_verifyDeviceOwnership(string memory _deviceDID, address _address) external view returns(bool) {
        if(devToHub[didToDevice[_deviceDID]] == ownerToHub[_address]){
            return true;
        } else {
            return false;
        }
    }

    function D2_verifySofthubOwnership(string memory _hubDID, address _address) external view returns(bool) {
        if(ownerToHub[_address] == didToSofthub[_hubDID]){
            return true;
        } else {
            return false;
        }
    }

    function D3_verifyDeviceToSofthub(string memory _softhubDID, string memory _deviceDID) external view returns(bool) {
        if(devToHub[didToDevice[_deviceDID]] == didToSofthub[_softhubDID]) {
            return true;
        } else {
            return false;
        }
    }

    function D4_verifyAdminToSofthub(string memory _softhubDID) external view returns(bool) {
        if(ownerToHub[msg.sender] == didToSofthub[_softhubDID]){
            if(keccak256(abi.encodePacked(dids[identityToAccount[msg.sender]].property)) == keccak256(abi.encodePacked("administrator"))){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
}
