// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.12;

import "./didfactory.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract IothubFactory is DidFactory{

    struct Device {
        uint deviceDID;
        uint ownerDID;
        string deviceType;
    }

    struct Iothub {
        uint softHubDID;
        string ownerDID;
        string name;
    }

    Iothub[] internal iothubs;
    Device[] internal devices;

    mapping (address => uint) internal ownerToHub; // owner account to hub from iothubs array
    mapping (string => uint) internal rnToDev; //registration Number to Device from devices array
    mapping (uint => uint) internal devToHub; // device appointed to a hub

    //create a new softHub
    function createSoftHub(string memory _name) external {
        //check account's identity to be administrator
        require(keccak256(abi.encodePacked(dids[identityToAccount[msg.sender]].property)) == keccak256(abi.encodePacked("administrator")));
        iothubs.push(Iothub(_createSoftHubDid(), dids[identityToAccount[msg.sender]].did, _name));
        ownerToHub[msg.sender] = iothubs.length - 1;  // !!Important!! each account can only have one softhub registered as administrator
    }

    //register a new device to the contract
    function registerDevice(string memory _type, string memory _registrationNo) external {
        devices.push(Device(_createDeviceDid(), 0, _type));
        rnToDev[_registrationNo] = devices.length - 1;
    }

    function showSoftHub() public view returns(Iothub memory){
        return iothubs[ownerToHub[msg.sender]];
    }

}