pragma solidity ^0.8.0;

contract VCFactory {

    uint proofDigits = 32;
    uint proofModulus = 100 ** proofDigits;
    // a nonce for generating psuedo proofs
    uint nonce = 0;

    struct Claim {
        string[] proof;
        string did;
    }

    Claim[] public claims;

    mapping (string => claims) public verifiableCredentials;

    function _createVC(string memory _did, uint proof) internal {
        claims.push(Claim(_generatePseudoProof, _did));
        
    }

    function _generatePseudoProof() internal returns(uint) {
        nonce++;
        return uint(keccak256(abi.encodePacked(now,msg.sender, nonce))) % proofModulus;
    }


}